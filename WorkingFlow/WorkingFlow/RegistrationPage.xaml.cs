﻿using SQLite;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using WorkingFlow.Table;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace WorkingFlow
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegistrationPage : ContentPage
    {
        public RegistrationPage()
        {
            InitializeComponent();
        }

        private  void Button_Clicked(object sender, EventArgs e)
        {
            var dbpath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "UserDatabase.db");
            var db = new SQLiteConnection(dbpath);
            db.CreateTable<RegUserTable>();

          
            var Username = userName.Text;
            var Password = pass.Text;
            var Email1 = email.Text;
            var PhoneNumber = phone.Text;
            bool Validate_user = IsValidate_username(Username);
            bool Validate_email = IsValidate_email(Email1);
            bool Validate_pass = IsValidate_pass(Password);
            bool Validate_phone= IsValidate_phonenumber(PhoneNumber);

            if (Validate_user && Validate_email && Validate_pass && Validate_phone)
            {
                var item = new RegUserTable()
                {
                    Username = userName.Text,
                    Password = pass.Text,
                    Email = email.Text,
                    Phonenumber = phone.Text
                };
                db.Insert(item);
                Device.BeginInvokeOnMainThread(async () =>
                {

                    var result = await this.DisplayAlert("Congratulations", "User Registered Sucessfully", "Yes", "Cancel");
                    if (result)
                        await Navigation.PushAsync(new LoginPage());

                });
            }
            else
            {
                Device.BeginInvokeOnMainThread(async () =>
                {

                    var result = await this.DisplayAlert("Error", "User Failed to Registered", "Yes", "Cancel");
                  
                });
            }
        }
        public bool IsValidate_username(String UserNname)
        {
            var usernamePattern = "^[A-Za-z]+$";
            if (!Regex.IsMatch(UserNname, usernamePattern))
            {

                ErrorLabel_1.Text = "UserName is InValid it should start with capital letter";
                return false;
            }
            return true;
               }
        public bool IsValidate_email(String Email)
        {
            var emailPattern = @"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$";
            if (!Regex.IsMatch(Email, emailPattern))
            {

                ErrorLabel_3.Text = "Email is InValid";
                return false;
            }
           
                return true;
                     
        }
        public bool IsValidate_pass(String Password)
        {
            var passwordPattern = "(?i)(?=.*[a-z])(?=.*[0-9])(?=.*[@#_])[a-z][a-z0-9@#_]{6,}[a-z0-9]$";
            if (!Regex.IsMatch(Password, passwordPattern))
            {

                ErrorLabel_2.Text = "At least one uppercase,lowercase,digit,specialcharacter and Minimum 8 in length";
                return false;
            }


            return true;


        }
        public bool IsValidate_phonenumber(String PhoneNumber)
        {
            var phonePattern = "[0-9]{10}";
            if (!Regex.IsMatch(PhoneNumber, phonePattern))
            {

                ErrorLabel_4.Text = "Phone number must contain 10 digit";
                return false;
            }
          return true;
       }
    }
}